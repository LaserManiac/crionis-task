use std::{
    cmp::Ordering,
    collections::HashSet,
};

use super::{
    point_set::{PointId, PointSet},
    vec2::Vec2,
};

pub struct Selection {
    points: HashSet<PointId>,
    sorted: Vec<PointId>,
    center: Option<Vec2>,
}

impl Selection {
    pub fn new() -> Self {
        Self {
            points: Default::default(),
            sorted: Default::default(),
            center: None,
        }
    }

    pub fn center(&self) -> Option<Vec2> {
        self.center
    }
    
    pub fn clear(&mut self) {
        self.points.clear();
        self.sorted.clear();
    }

    pub fn select_closest(&mut self, origin: Vec2, radius: f32, set: &PointSet) {
        if let Some(closest) = set.find_closest(origin, radius) {
            self.points.insert(closest);
            self.update(set);
        }
    }

    pub fn select_rect(&mut self, from: Vec2, to: Vec2, set: &PointSet) {
        let old_len = self.points.len();
        self.points.extend(set.find_in_rect(from, to));
        if self.points.len() != old_len {
            self.update(set);
        }
    }

    pub fn iter_full<'a, 'b: 'a>(&'a self, set: &'b PointSet)
        -> impl Iterator<Item=(PointId, &'b Vec2)> + 'a {
        self.sorted.iter()
            .filter_map(move |id| set.get(*id)
                .map(|point| (*id, point)))
    }
    
    pub fn update(&mut self, set: &PointSet) {
        self.remove_deleted(set);
        self.sort(set);
    }

    fn update_center(&mut self, set: &PointSet) {
        if self.points.len() == 0 {
            self.center = None;
            return;
        }

        let init = (Vec2::new(0.0, 0.0), 0usize);
        let (sum, count) = self.points.iter()
            .filter_map(|id| set.get(*id))
            .fold(init, |(acc, count), point| (acc + *point, count + 1));
        let center = sum / count as f32;
        self.center = Some(center);
    }
    
    fn remove_deleted(&mut self, set: &PointSet) {
        let new = HashSet::with_capacity(self.points.len());
        let old = std::mem::replace(&mut self.points, new);
        let filtered = old.into_iter().filter(|id| set.has(*id));
        self.points.extend(filtered);
    }

    fn sort(&mut self, set: &PointSet) {
        self.update_center(set);
        if let Some(center) = self.center {
            self.sorted = Vec::with_capacity(self.points.len());
            self.sorted.extend(self.points.iter().filter(|id| set.has(**id)));
            self.sorted.sort_by(|a, b| {
                let a = *set.get(*a).unwrap() - center;
                let b = *set.get(*b).unwrap() - center;
                cmp_ccw_90deg(&a, &b).reverse()
            });
        } else {
            self.sorted.clear();
        }
    }
}


// Compare using cross product.
// If the vectors are collinear, the shorter one comes first. 
fn cmp_relative(a: &Vec2, b: &Vec2) -> Ordering {
    let cross = a.cross(b);
    if cross > f32::MIN_POSITIVE {
        Ordering::Less
    } else if cross < -f32::MIN_POSITIVE {
        Ordering::Greater
    } else {
        let a = a.len2();
        let b = b.len2();
        a.partial_cmp(&b).unwrap_or(Ordering::Equal)
    }
}


// Compare using the cross product method,
// but the vectors are ordered starting at 90 degrees (12 o' clock).
fn cmp_ccw_90deg(a: &Vec2, b: &Vec2) -> Ordering {
    if a.x > 0.0 && b.x <= 0.0 {
        Ordering::Greater
    } else if a.x <= 0.0 && b.x > 0.0 {
        Ordering::Less
    } else if a.x == 0.0 && b.x == 0.0 {
        if a.y >= 0.0 || b.y >= 0.0 {
            a.y.partial_cmp(&b.y)
                .unwrap_or(Ordering::Equal)
        } else {
            b.y.partial_cmp(&a.y)
                .unwrap_or(Ordering::Equal)
        }
    } else {
        cmp_relative(a, b)
    }
}
