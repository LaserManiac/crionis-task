#[derive(Copy, Clone, Default)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

impl Vec2 {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }

    pub fn len2(&self) -> f32 {
        self.x.powi(2) * self.y.powi(2)
    }

    pub fn len(&self) -> f32 {
        f32::sqrt(self.len2())
    }

    pub fn cross(self, other: &Vec2) -> f32 {
        self.x * other.y - self.y * other.x
    }

    pub fn is_in_rect(&self, from: Vec2, to: Vec2) -> bool {
        self.x >= from.x
            && self.y >= from.y
            && self.x <= to.x
            && self.y <= to.y
    }
}

impl std::ops::Add<Self> for Vec2 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Vec2::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl std::ops::Sub<Self> for Vec2 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Vec2::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl std::ops::Div<f32> for Vec2 {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        Vec2::new(self.x / rhs, self.y / rhs)
    }
}
