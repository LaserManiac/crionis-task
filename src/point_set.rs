use slotmap::{new_key_type, SlotMap};

use super::vec2::Vec2;

new_key_type! { pub struct PointId; }

pub struct PointSet {
    points: SlotMap<PointId, Vec2>,
}

impl PointSet {
    pub fn new() -> Self {
        Self {
            points: Default::default(),
        }
    }

    pub fn create(&mut self, point: Vec2) -> PointId {
        self.points.insert(point)
    }

    pub fn remove(&mut self, id: PointId) -> Option<Vec2> {
        self.points.remove(id)
    }

    pub fn has(&self, id: PointId) -> bool {
        self.points.contains_key(id)
    }

    pub fn get(&self, id: PointId) -> Option<&Vec2> {
        self.points.get(id)
    }

    pub fn find_closest(&self, origin: Vec2, radius: f32) -> Option<PointId> {
        let mut closest = None;
        let mut min_dst = radius;
        for (id, point) in self.points.iter() {
            let cur_dst = (*point - origin).len();
            if cur_dst < min_dst {
                closest = Some(id);
                min_dst = cur_dst;
            }
        }
        return closest;
    }

    pub fn find_in_rect(&self, from: Vec2, to: Vec2) -> impl Iterator<Item=PointId> + '_ {
        self.points.iter()
            .filter(move |(_, point)| point.is_in_rect(from, to))
            .map(|(id, _)| id)
    }
}
