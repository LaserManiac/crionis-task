use point_set::*;
use vec2::*;
use selection::*;

mod point_set;
mod selection;
mod vec2;

fn main() {
    let mut points = PointSet::new();
    points.create(Vec2::new(-2.0, -6.0));
    points.create(Vec2::new(7.0, -4.0));
    points.create(Vec2::new(2.0, 1.0));
    points.create(Vec2::new(-1.72737, -1.72737));
    points.create(Vec2::new(-3.0, 9.0));
    points.create(Vec2::new(10.0, 11.0));
    points.create(Vec2::new(200.0, 100.0));
    let to_remove = points.create(Vec2::new(5.0, -2.0));

    let mut selection = Selection::new();
    selection.select_closest(Vec2::new(10.0, 10.0), 2.0, &points);
    selection.select_rect(Vec2::new(-10.0, -10.0), Vec2::new(10.0, 10.0), &points);
    
    points.remove(to_remove);
    selection.update(&points);
    
    let center = selection.center().unwrap_or_default();
    println!("Center: {}, {}", center.x, center.y);
    selection.iter_full(&points)
        .map(|(_, point)| *point - center)
        .for_each(print_point);

    selection.clear();
    assert_eq!(0, selection.iter_full(&points).count());
}

fn print_point(point: Vec2) {
    let angle = f32::atan2(point.y, point.x).to_degrees().round() as i32;
    let angle = (450 - angle) % 360;
    println!("{:3}° {:6.3}, {:6.3}", angle, point.x, point.y);
}
